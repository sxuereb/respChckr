package chckr

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestDNSLookup(t *testing.T) {
	connStart := time.Now()
	cases := []struct {
		desc         string
		trace        httpTrace
		zeroExpected bool
		expectedDiff time.Duration
	}{
		{
			desc: "No DNS lookup",
			trace: httpTrace{
				connStart: connStart,
			},
			zeroExpected: true,
		},
		{
			desc: "DNS lookup took 1ms",
			trace: httpTrace{
				connStart: connStart,
				dnsStart:  connStart.Add(-1 * time.Millisecond),
			},
			expectedDiff: 1 * time.Millisecond,
		},
	}

	for _, c := range cases {
		t.Run(c.desc, func(t *testing.T) {
			lookup := c.trace.dnsLookup()

			if c.zeroExpected {
				require.Zero(t, lookup)
				return
			}

			require.Equal(t, c.expectedDiff, lookup)
		})
	}
}

func TestTLSHandshake(t *testing.T) {
	tlsStart := time.Now()
	trace := httpTrace{
		tlsStart: tlsStart,
		tlsDone:  tlsStart.Add(2 * time.Millisecond),
	}

	require.Equal(t, 2*time.Millisecond, trace.tlsHandshake())
}

func TestServerProcessing(t *testing.T) {
	connStart := time.Now()
	trace := httpTrace{
		connStart: connStart,
		firstByte: connStart.Add(1 * time.Millisecond),
	}

	require.Equal(t, 1*time.Millisecond, trace.serverProcessing())
}

func TestContentTransfer(t *testing.T) {
	firstByte := time.Now()
	trace := httpTrace{
		firstByte: firstByte,
		done:      firstByte.Add(1 * time.Millisecond),
	}

	require.Equal(t, 1*time.Millisecond, trace.contentTransfer())
}

func TestDone(t *testing.T) {
	dnsStart := time.Now()
	trace := httpTrace{
		dnsStart: dnsStart,
		done:     dnsStart.Add(4 * time.Millisecond),
	}

	require.Equal(t, 4*time.Millisecond, trace.total())
}

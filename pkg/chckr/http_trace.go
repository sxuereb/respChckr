package chckr

import "time"

// httpTrace has all the timers for "start" and "done" of different parts of an
// http call.
type httpTrace struct {
	dnsStart, connStart, connDone, gotConn, firstByte, tlsStart, tlsDone, done time.Time
}

// dnsLookup calculates the time the request to do a DNS lookup from the start of
// the connection to the start of the connection.
func (t *httpTrace) dnsLookup() time.Duration {
	// Check if we skipped DNS.
	if t.dnsStart.IsZero() {
		return 0 * time.Nanosecond
	}

	return t.connStart.Sub(t.dnsStart) / time.Nanosecond
}

// tlsHandshake calculates how long it takes to do a TLS handshake.
func (t *httpTrace) tlsHandshake() time.Duration {
	return t.tlsDone.Sub(t.tlsStart) / time.Nanosecond
}

// serverProcessing calculates the different between the start of the connection
// and the first byte received.
func (t *httpTrace) serverProcessing() time.Duration {
	return t.firstByte.Sub(t.connStart) / time.Nanosecond
}

// contentTransfer calculates how long it took for the content to reach the
// application by calculating the time from the first byte to the done.
func (t *httpTrace) contentTransfer() time.Duration {
	return t.done.Sub(t.firstByte) / time.Nanosecond
}

// total calculates the total time of the request from the dns request to the
// reading of the body.
func (t *httpTrace) total() time.Duration {
	return t.done.Sub(t.dnsStart) / time.Nanosecond
}

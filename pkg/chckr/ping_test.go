package chckr

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

// Buffer is the same a bytes.Buffer just thread safe.
type Buffer struct {
	b bytes.Buffer
	m sync.Mutex
}

func (b *Buffer) Write(p []byte) (n int, err error) {
	b.m.Lock()
	defer b.m.Unlock()
	return b.b.Write(p)
}

func (b *Buffer) String() string {
	b.m.Lock()
	defer b.m.Unlock()
	return b.b.String()
}

func TestPing(t *testing.T) {
	var ticks int
	ts := httptest.NewTLSServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ticks++
		w.Write([]byte{})
	}))
	defer ts.Close()

	report := Buffer{}
	reqURL, err := url.Parse(ts.URL)
	if err != nil {
		require.NoError(t, err, "failed to parse mock server URL: %s", ts.URL)
	}

	err = Ping(*reqURL, 5*time.Second, 1*time.Second, &report)
	require.NoError(t, err)

	lines := strings.Split(report.String(), "\n")
	require.Len(t, lines, 6)
	require.Equal(t, 5, ticks)
}

package chckr

import (
	"context"
	"crypto/tls"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptrace"
	"net/url"
	"strconv"
	"time"
)

// Ping sends a GET request to the specified site at a frequency for a duration
// of time. The specified writer needs to be thread safe. This adds the request
// timestamp as a query parameter to prevent any caching whilst also ignoring
// the keep-alive and opening a new connection every tick.
func Ping(reqURL url.URL, dur, freq time.Duration, w io.Writer) error {
	q := reqURL.Query()
	q.Set("date", strconv.FormatInt(time.Now().UTC().Unix(), 10))
	reqURL.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, reqURL.String(), nil)
	if err != nil {
		return err
	}

	finish := time.Now().Add(dur)

	for time.Now().Before(finish) || time.Now().Equal(finish) {
		go func() {
			resp, hTrace, err := traceReq(req)
			if err != nil {
				fmt.Fprintf(w, "failed to read response: %v\n", err)
				return
			}

			report(w, hTrace, resp)
		}()

		time.Sleep(freq)
	}

	return nil
}

// traceReq creates a client and a tracer for the specified request and
// returns the response and an httpTrace timers.
func traceReq(req *http.Request) (*http.Response, httpTrace, error) {
	hTrace := httpTrace{}

	trace := &httptrace.ClientTrace{
		DNSStart: func(_ httptrace.DNSStartInfo) {
			hTrace.dnsStart = time.Now()
		},
		DNSDone: func(_ httptrace.DNSDoneInfo) {
			hTrace.connStart = time.Now()
		},
		ConnectStart: func(_, _ string) {
			// In case host is an IP
			if hTrace.connStart.IsZero() {
				hTrace.connStart = time.Now()
			}
		},
		TLSHandshakeStart: func() {
			hTrace.tlsStart = time.Now()
		},
		TLSHandshakeDone: func(_ tls.ConnectionState, _ error) {
			hTrace.tlsDone = time.Now()
		},
		GotConn: func(_ httptrace.GotConnInfo) {
			hTrace.gotConn = time.Now()
		},
		ConnectDone: func(_, _ string, _ error) {
			hTrace.connDone = time.Now()
		},
		GotFirstResponseByte: func() { hTrace.firstByte = time.Now() },
	}

	req = req.WithContext(httptrace.WithClientTrace(context.Background(), trace))
	tr := http.Transport{
		DisableKeepAlives: true,
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
	}

	client := http.Client{
		Transport: &tr,
	}

	resp, err := client.Do(req)
	if err != nil {
		return nil, httpTrace{}, err
	}

	if _, err := io.Copy(ioutil.Discard, resp.Body); err != nil {
		return nil, httpTrace{}, err
	}

	if err := resp.Body.Close(); err != nil {
		return nil, httpTrace{}, err
	}

	// We got all of the body contents here.
	hTrace.done = time.Now()

	return resp, hTrace, nil
}

// report writes all the diffs between each step of the HTTP protocol to the
// provided io.Writer.
func report(w io.Writer, hTrace httpTrace, resp *http.Response) {

	fmt.Fprintf(
		w,
		`Protocol=%s Status="%s" DNSLookup=%s TLSHandshake=%s ServerProcessing=%s ContentTransfer=%s Total=%s`+"\n",
		resp.Proto,
		resp.Status,
		hTrace.dnsLookup(),
		hTrace.tlsHandshake(),
		hTrace.serverProcessing(),
		hTrace.contentTransfer(),
		hTrace.total(),
	)
}

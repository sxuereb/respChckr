CURRENT_DIR=$(shell pwd)
GOARCH?=amd64
GOOS?=linux
TAG?=latest

build:
	docker run --rm \
	-v ${CURRENT_DIR}:/go/src/gitlab.com/SteveAzz/respChckr \
	-w /go/src/gitlab.com/SteveAzz/respChckr \
	-e GOOS=${GOOS} \
	-e GOARCH=${GOARCH} \
	golang:1.10-alpine \
	go build -o dist/${GOOS}/${GOARCH}/respChckr cmd/cli/cli.go

test:
	docker run --rm \
	-v ${CURRENT_DIR}:/go/src/gitlab.com/SteveAzz/respChckr \
	-w /go/src/gitlab.com/SteveAzz/respChckr \
	golang:1.10-stretch \
	/bin/sh -c 'go test -race -v $$(go list ./... | grep -v "/vendor/")'

docker-build:
	docker build -t steveazz/respchckr:${TAG} .

lint:
	golint $$(go list ./... | grep -v /vendor/)

vet:
	go vet $$(go list ./... | grep -v /vendor/)

.PHONY: test lint vet

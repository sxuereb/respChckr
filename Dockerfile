FROM golang:1.10-alpine as builder
WORKDIR /go/src/gitlab.com/SteveAzz/respChckr/
COPY . .
RUN go build -o app cmd/cli/cli.go

FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=builder /go/src/gitlab.com/SteveAzz/respChckr/app .
CMD ["./app"]


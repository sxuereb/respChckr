# RespChckr

Small GO application that will report the HTTP response times over the specified duration for the specified website from your location.

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/SteveAzz/respChckr)](https://goreportcard.com/report/gitlab.com/SteveAzz/respChckr)
[![pipeline status](https://gitlab.com/SteveAzz/respChckr/badges/master/pipeline.svg)](https://gitlab.com/SteveAzz/respChckr/commits/master)
[![MIT Licence](https://badges.frapsoft.com/os/mit/mit.svg?v=103)](https://opensource.org/licenses/mit-license.php)

## Build

Build for linux amd64

```shell
$ make build
```

Build for darwin amd64

```shell
$ make build GOOS=darwin
```

Docker

````shell
$ make docker-build
````

## Run

### Locally
```shell
$ ./dist/darwin/amd64/respChckr -h

USAGE
 ./dist/darwin/amd64/respChckr [flags]

FLAGS
  -duration 5m0s            Duration of how long the rspChckr should run.
  -frequency 10s            How often should the application send a request to the site.
  -site https://gitlab.com  Website to check response status on.
```

```shell

$ ./dist/darwin/amd64/respChckr

Protocol=HTTP/1.1 Status="200 OK" DNSLookup=11.112752ms TLSHandshake=430.44178ms ServerProcessing=862.767589ms ContentTransfer=2.011567ms Total=875.891908ms
Protocol=HTTP/1.1 Status="200 OK" DNSLookup=66.321152ms TLSHandshake=431.183614ms ServerProcessing=865.219436ms ContentTransfer=2.059468ms Total=933.600056ms
```

### Docker

```
$ docker run --rm registry.gitlab.com/steveazz/respchckr ./app -h

USAGE
 ./app [flags]

FLAGS
  -duration 5m0s            Duration of how long the rspChckr should run.
  -frequency 10s            How often should the application send a request to the site.
  -site https://gitlab.com  Website to check response status on.
```

```shell
$ docker run --rm registry.gitlab.com/steveazz/respchckr
```

## Test

```shell
$ make test
```

## Linting

```shell
$ make lint
$ make vet
```

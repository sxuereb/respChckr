package main

import (
	"flag"
	"fmt"
	"net/url"
	"os"
	"text/tabwriter"
	"time"

	"gitlab.com/SteveAzz/respChckr/pkg/chckr"
)

func main() {
	fs := flag.NewFlagSet("respChckr", flag.ExitOnError)
	var (
		duration  = fs.Duration("duration", 5*time.Minute, "Duration of how long the rspChckr should run.")
		site      = fs.String("site", "https://gitlab.com", "Website to check response status on.")
		frequency = fs.Duration("frequency", 10*time.Second, "How often should the application send a request to the site.")
	)
	fs.Usage = usageFor(fs, os.Args[0]+" [flags]")
	if err := fs.Parse(os.Args[1:]); err != nil {
		fmt.Fprintf(os.Stderr, "failed to pase flags: %v", err)
		fs.Usage()
		os.Exit(1)
	}

	reqURL, err := url.Parse(*site)
	if err != nil {
		fmt.Fprintf(os.Stderr, "-site failed to parse: %v\n", err)
		os.Exit(1)
	}

	if err := chckr.Ping(*reqURL, *duration, *frequency, os.Stdout); err != nil {
		fmt.Fprintf(os.Stderr, "failed to send request to site: %v\n", err)
		os.Exit(1)
	}
}

func usageFor(fs *flag.FlagSet, short string) func() {
	return func() {
		fmt.Printf("USAGE\n")
		fmt.Printf(" %s\n", short)
		fmt.Printf("\n")
		fmt.Printf("FLAGS\n")
		w := tabwriter.NewWriter(os.Stdout, 0, 2, 2, ' ', 0)
		fs.VisitAll(func(f *flag.Flag) {
			fmt.Fprintf(w, "\t-%s %s\t%s\n", f.Name, f.DefValue, f.Usage)
		})
		err := w.Flush()
		if err != nil {
			fmt.Fprintf(os.Stderr, "failed to flush tab write: %v", err)
			os.Exit(1)
		}
		fmt.Printf("\n")
	}
}
